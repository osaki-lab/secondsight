package secondsight

import (
	"context"
	"fmt"
	"log"
	"net/http"
	"strings"

	"nhooyr.io/websocket"
	"nhooyr.io/websocket/wsjson"
)

// Receiver is struct to interface physical connection and this library
type Receiver interface {
	ReceiveLogUpdate(channelName string, logs []LogEntry, scrollID int)
	ReceiveOldLogs(channelName string, logs []LogEntry, scrollID int)
	ReceiveChannelUpdate(channels []string, containers []*ContainerConfig)
}

// ReceivedEvent is record of DummyReceiver
type ReceivedEvent struct {
	Type        EventType
	ChannelName string
	Logs        []LogEntry
	ScrollID    int
	Channels    []string
	Containers  []*ContainerConfig
}

func (r ReceivedEvent) String() string {
	switch r.Type {
	case LogUpdateEvent:
		var logs []interface{}
		for _, log := range r.Logs {
			logs = append(logs, log.Content)
		}
		return fmt.Sprintf("[new-log: channel=%s logs=[%v]]", r.ChannelName, logs)
	case ReceiveOldLogsEvent:
		var logs []interface{}
		for _, log := range r.Logs {
			logs = append(logs, log.Content)
		}
		return fmt.Sprintf("[old-log: channel=%s logs=[%v]]", r.ChannelName, logs)
	case ChannelUpdateEvent:
		return fmt.Sprintf("[channel: [%s]]", strings.Join(r.Channels, ", "))
	}
	panic(r)
}

// DummyReceiver is a mock object of ClientConnection
type DummyReceiver struct {
	events []ReceivedEvent
}

func (d *DummyReceiver) GetEvents() []ReceivedEvent {
	result := d.events
	d.events = nil
	return result
}

func (d *DummyReceiver) ReceiveLogUpdate(channelName string, logs []LogEntry, scrollID int) {
	d.events = append(d.events, ReceivedEvent{
		Type:        LogUpdateEvent,
		ChannelName: channelName,
		Logs:        logs,
		ScrollID:    scrollID,
	})
}

func (d *DummyReceiver) ReceiveOldLogs(channelName string, logs []LogEntry, scrollID int) {
	d.events = append(d.events, ReceivedEvent{
		Type:        ReceiveOldLogsEvent,
		ChannelName: channelName,
		Logs:        logs,
		ScrollID:    scrollID,
	})
}

func (d *DummyReceiver) ReceiveChannelUpdate(channels []string, containers []*ContainerConfig) {
	d.events = append(d.events, ReceivedEvent{
		Type:       ChannelUpdateEvent,
		Channels:   channels,
		Containers: containers,
	})
}

var _ Receiver = &DummyReceiver{}

// JsonMessage is response via client/server
//
// Basic Command is Here:
//
// (client)channels: RetrieveLogs channel information
// (client)connect: RetrieveLogs logs and keep listening
// (client)disconnect: Stop receive update
// (client)logs: Get old logs
// (server)channels: Channel information
// (server)old-logs: Log requested
// (server)update: New arrival logs
// (server)close: Channel is closed
type JsonMessage struct {
	Command    string             `json:"cmd"`
	Channel    string             `json:"chan,omitempty"`
	Query      map[string]string  `json:"query,omitempty"`
	Logs       []LogEntry         `json:"logs,omitempty"`
	Containers []*ContainerConfig `json:"containers,omitempty"`
	Channels   []string           `json:"channels,omitempty"`
	Count      int                `json:"count,omitempty"`
	ScrollID   int                `json:"scrollID,omitempty"`
}

type WebSocketConnection struct {
	conn *websocket.Conn
	ctx  context.Context
}

func (c WebSocketConnection) ReceiveLogUpdate(channelName string, logs []LogEntry, scrollID int) {
	wsjson.Write(context.Background(), c.conn, &JsonMessage{
		Command:  "new-log",
		Channel:  channelName,
		Logs:     logs,
		ScrollID: scrollID,
	})
}

func (c WebSocketConnection) ReceiveOldLogs(channelName string, logs []LogEntry, scrollID int) {
	wsjson.Write(context.Background(), c.conn, &JsonMessage{
		Command:  "old-log",
		Channel:  channelName,
		Logs:     logs,
		ScrollID: scrollID,
	})
}

func (c WebSocketConnection) ReceiveChannelUpdate(channels []string, containers []*ContainerConfig) {
	err := wsjson.Write(context.Background(), c.conn, &JsonMessage{
		Command:    "container-list",
		Channels:   channels,
		Containers: containers,
	})
	if err != nil {
		log.Println("💣 ", err)
	}
}

func (c WebSocketConnection) Wait() {
	<-c.ctx.Done()
}

func NewWebSocketConnection(w http.ResponseWriter, r *http.Request, logStore *LogStore) (*WebSocketConnection, *ClientConnection, error) {
	ctx := r.Context()
	conn, err := websocket.Accept(w, r, &websocket.AcceptOptions{
		Subprotocols:         nil,
		InsecureSkipVerify:   true,
		OriginPatterns:       nil,
		CompressionMode:      0,
		CompressionThreshold: 0,
	})

	if err != nil {
		return nil, nil, err
	}
	wsConn := &WebSocketConnection{
		conn: conn,
		ctx:  ctx,
	}
	clientConn := NewClientConnection(wsConn, logStore)

	go func() {
		for {
			var req JsonMessage
			err := wsjson.Read(ctx, conn, &req)
			if err != nil {
				log.Println(err)
				conn.Close(http.StatusOK, "OK")
				return
			}
			switch req.Command {
			case "channels":
				clientConn.RetrieveChannels()
			case "disconnect":
				clientConn.Disconnect(req.Channel)
			case "connect":
				clientConn.Listen(req.Channel, req.Query)
			case "old-log":
				clientConn.RetrieveLogs(req.Channel)
			}
		}
	}()

	return wsConn, clientConn, nil
}

var _ Receiver = &WebSocketConnection{}
