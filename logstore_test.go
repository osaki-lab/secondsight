package secondsight

import (
	"log"
	"testing"

	"github.com/stretchr/testify/assert"
)

const dummyContainerID = "container"

func init() {
	log.SetFlags(log.LstdFlags | log.Lshortfile)
	log.SetPrefix("🐙 ")
}

func getLatestLog(s *LogStore) interface{} {
	c := s.containers[dummyContainerID]
	return c.logs[len(c.logs)-1].Content
}

type dummyData struct {
	data int
}

func insertFixture(l *LogStore, count int) {
	for i := 0; i < count; i++ {
		l.AppendLog(dummyContainerID, LogEntry{Content: i})
	}
}

func isOdd(entry interface{}) bool {
	return entry.(*dummyData).data%2 == 1
}

func TestLogList_SearchFromNewToOld(t *testing.T) {
	l := NewLogStore(nil)
	l.RegisterContainer(&ContainerConfig{
		Name:      "test container",
		Active:    true,
		ID:        dummyContainerID,
		Path:      "/bin/bash",
		CreatedAt: 0,
	})
	insertFixture(l, 100)

	tests := []struct {
		name         string
		searchFrom   int
		count        int
		match        func(entry interface{}) bool
		wantErr      bool
		wantCount    int
		wantScrollID int
	}{
		{
			name:         "standard search from newest result",
			searchFrom:   -1,
			count:        10,
			match:        nil,
			wantErr:      false,
			wantCount:    10,
			wantScrollID: 90,
		},
		{
			name:         "standard search from newest result until last",
			searchFrom:   -1,
			count:        100,
			match:        nil,
			wantErr:      false,
			wantCount:    100,
			wantScrollID: 0,
		},
		{
			name:         "standard search from newest result until last",
			searchFrom:   -1,
			count:        100,
			match:        nil,
			wantErr:      false,
			wantCount:    100,
			wantScrollID: 0,
		},
		{
			name:         "standard search from newest result over last",
			searchFrom:   -1,
			count:        150,
			match:        nil,
			wantErr:      false,
			wantCount:    100,
			wantScrollID: 0,
		},
		{
			name:         "standard search odd from newest",
			searchFrom:   -1,
			count:        5,
			match:        isOdd,
			wantErr:      false,
			wantCount:    5,
			wantScrollID: 91,
		},
		{
			name:         "standard search only odd from newest until last",
			searchFrom:   -1,
			count:        50,
			match:        isOdd,
			wantErr:      false,
			wantCount:    50,
			wantScrollID: 1,
		},
		{
			name:         "standard search only odd from newest over last",
			searchFrom:   -1,
			count:        51,
			match:        isOdd,
			wantErr:      false,
			wantCount:    50,
			wantScrollID: 0,
		},
		{
			name:         "standard search from specific item",
			searchFrom:   10,
			count:        8,
			match:        nil,
			wantErr:      false,
			wantCount:    8,
			wantScrollID: 2,
		},
		{
			name:         "standard search from specific item at last edge",
			searchFrom:   0,
			count:        10,
			match:        nil,
			wantErr:      false,
			wantCount:    0,
			wantScrollID: 0,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			result, scrollIDs, err := l.Search(dummyContainerID, tt.searchFrom, tt.count, Option{
				Match:     tt.match,
				Direction: NewToOld,
			})
			if tt.wantErr {
				assert.Error(t, err)
			} else {
				assert.NoError(t, err)
				assert.Len(t, result, tt.wantCount, "count")
				assert.Equal(t, tt.wantScrollID, scrollIDs.ToOld, "scrollID")
			}
		})
	}
}

