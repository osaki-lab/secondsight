package secondsight

import "strings"

type Severity uint

var str2level = map[string]Severity{
	"TRACE":    TraceLevel,
	"DEBUG":    DebugLevel,
	"INFO":     InfoLevel,
	"WARNING":  WarningLevel,
	"WARN":     WarningLevel,
	"ERROR":    ErrorLevel,
	"FATAL":    CriticalLevel,
	"CRITICAL": CriticalLevel,
}

// pino style
// https://getpino.io/
var num2level = map[int]Severity{
	10: TraceLevel,
	20: DebugLevel,
	30: InfoLevel,
	40: WarningLevel,
	50: ErrorLevel,
	60: CriticalLevel,
}

func ParseLevel(src string, defaultLevel Severity) Severity {
	if level, ok := str2level[strings.ToUpper(src)]; ok {
		return level
	}
	return defaultLevel
}

const (
	TraceLevel    Severity = 1
	DebugLevel    Severity = 2
	InfoLevel     Severity = 3
	WarningLevel  Severity = 4
	ErrorLevel    Severity = 5
	CriticalLevel Severity = 6
)

func (s Severity) String() string {
	switch s {
	case TraceLevel:
		return "1"
	case DebugLevel:
		return "2"
	case InfoLevel:
		return "3"
	case WarningLevel:
		return "4"
	case ErrorLevel:
		return "5"
	case CriticalLevel:
		return "6"
	}
	panic(s)
}

type DockerEventType string

const (
	ContainerStartEvent   DockerEventType = "start"
	ContainerRunningEvent DockerEventType = "running"
	ContainerDieEvent     DockerEventType = "die"
)

type LogEventType string

const (
	StatLogEvent    LogEventType = "s"
	StructLogEvent  LogEventType = "l"
	TraceLogEvent   LogEventType = "t"
	MetricsLogEvent LogEventType = "m"
)

type Direction int

const (
	NewToOld Direction = 0
	OldTONew Direction = 1
	Both     Direction = 2
)

// EventType is for DummyReceiver's event
type EventType int

const (
	LogUpdateEvent EventType = iota + 1
	ReceiveOldLogsEvent
	ChannelUpdateEvent
)
