package secondsight

import (
	"context"
	"errors"
	"sync"
	"time"
)

var ErrContainerNotFound = errors.New("container not found")

type LogType string

const (
	StdOutLog LogType = "stdout"
	StdErrLog LogType = "stderr"
)

type Container struct {
	LogOffset int
	logs      []LogEntry
	config    *ContainerConfig
	ctx       context.Context
	cancel    context.CancelFunc
}

type LogStore struct {
	containers  map[string]*Container
	connections *ClientConnections
	lock        *sync.RWMutex
}

func NewLogStore(connections *ClientConnections) *LogStore {
	return &LogStore{
		containers:  make(map[string]*Container),
		lock:        &sync.RWMutex{},
		connections: connections,
	}
}

func (ls *LogStore) Iterate(callback func(containerID string, container *Container)) {
	ls.lock.RLock()
	defer ls.lock.RUnlock()
	for containerID, container := range ls.containers {
		callback(containerID, container)
	}
}

func (ls *LogStore) AppendLog(containerID string, logEntry LogEntry) error {
	ls.lock.Lock()
	c, ok := ls.containers[containerID]
	if !ok {
		ls.lock.Unlock()
		return ErrContainerNotFound
	}
	c.logs = append(c.logs, logEntry)
	ls.lock.Unlock()
	ls.connections.ReceiveLogUpdate(containerID)
	return nil
}

func (ls *LogStore) RegisterContainer(cc *ContainerConfig) {
	ls.lock.Lock()
	c, ok := ls.containers[cc.ID]
	if ok {
		c.config.Active = cc.Active
	} else {
		ctx, cancel := context.WithCancel(context.Background())
		ls.containers[cc.ID] = &Container{
			config: cc,
			ctx:    ctx,
			cancel: cancel,
			logs:   make([]LogEntry, 0, 10000),
		}
	}
	ls.lock.Unlock()
	timestamp := time.Now().UnixNano()
	if ls.connections != nil {
		ls.connections.Loop(func(c *ClientConnection) {
			c.updateChannels(timestamp, []string{cc.ID})
		})
	}
}

func (ls *LogStore) SetContainerState(containerID string, running bool) error {
	ls.lock.Lock()
	c, ok := ls.containers[containerID]
	if !ok {
		ls.lock.Unlock()
		return ErrContainerNotFound
	}
	c.config.Active = running
	ls.lock.Unlock()
	if !running {
		c.cancel()
	}
	return nil
}

func (ls *LogStore) Search(containerID string, searchFrom, count int, opt Option) (result []LogEntry, scrollIDs ScrollIDs, err error) {
	ls.lock.RLock()
	defer ls.lock.RUnlock()

	result = make([]LogEntry, 0, count)

	c, ok := ls.containers[containerID]
	if !ok {
		err = ErrContainerNotFound
		return
	}
	if len(c.logs) == 0 {
		return
	}
	var includeEdge bool
	if searchFrom == -1 {
		searchFrom = len(c.logs) - 1
		includeEdge = true
	} else {
		includeEdge = false
	}

	if opt.Direction == NewToOld {
		ls.searchNewToOld(c, &result, &scrollIDs, searchFrom, count, opt.Match, includeEdge)
	} else if opt.Direction == OldTONew {
		ls.searchOldToNew(c, &result, &scrollIDs, searchFrom, count, opt.Match)
	} else {
		ls.searchNewToOld(c, &result, &scrollIDs, searchFrom, count/2, opt.Match, true)
		ls.searchOldToNew(c, &result, &scrollIDs, searchFrom, count-len(result), opt.Match)
	}
	return
}

func (ls *LogStore) searchNewToOld(c *Container, result *[]LogEntry, scrollIDs *ScrollIDs, logIndex, count int, match func(entry interface{}) bool, includeEdge bool) {
	if !includeEdge {
		logIndex--
	}
	lastLogIndex := logIndex
	tmpResult := make([]LogEntry, 0, count)

	for len(tmpResult) < count && logIndex >= 0 {
		entry := c.logs[logIndex]
		if match == nil || match(entry.Content) {
			tmpResult = append(tmpResult, entry)
			lastLogIndex = logIndex
		}
		logIndex--
	}
	for i := 0; i < len(tmpResult)/2; i++ {
		tmpResult[i], tmpResult[len(tmpResult)-i-1] = tmpResult[len(tmpResult)-i-1], tmpResult[i]
	}
	*result = append(*result, tmpResult...)
	if logIndex == -1 {
		lastLogIndex = 0
	}
	scrollIDs.ToOld = lastLogIndex
	return
}

func (ls *LogStore) searchOldToNew(c *Container, result *[]LogEntry, scrollIDs *ScrollIDs, logIndex, count int, match func(entry interface{}) bool) {
	lastLogIndex := logIndex
	logIndex++

	for (len(*result) < count) && (logIndex < len(c.logs)) {
		entry := c.logs[logIndex]
		if match == nil || match(entry) {
			*result = append(*result, entry)
		}
		lastLogIndex = logIndex
		logIndex++
	}
	scrollIDs.ToNew = lastLogIndex
	return
}

func (ls *LogStore) listen(config *ContainerConfig, events <-chan LogEntry) {
	ls.RegisterContainer(config)
	for e := range events {
		ls.AppendLog(config.ID, e)
	}
}

func (ls *LogStore) ConnectToDocker(ctx context.Context, d Docker) error {
	events, err := d.Events(ctx)
	if err != nil {
		return err
	}
	for e := range events {
		switch e.EventType {
		case ContainerRunningEvent:
			go ls.listen(e.Config, e.LogStream)
		case ContainerStartEvent:
			go ls.listen(e.Config, e.LogStream)
		case ContainerDieEvent:
			if ls.connections != nil {
				ls.SetContainerState(e.ContainerID, false)
			}
		}
	}
	return nil
}
