package secondsight

import (
	"strconv"
	"time"
)

type SerializedString string

func (s SerializedString) MarshalJSON() ([]byte, error) {
	return []byte(s), nil
}

type Timestamp int64

func TimestampFromTime(t time.Time) Timestamp {
	return Timestamp(t.UnixNano())
}

func (t Timestamp) MarshalJSON() ([]byte, error) {
	v := strconv.Itoa(int(int64(t) / int64(time.Millisecond/time.Nanosecond)))
	return []byte(v), nil
}

type StructLogEntry struct {
	TraceID   string           `json:"tid"`
	SpanID    string           `json:"sid"`
	Timestamp Timestamp        `json:"ts"`
	Severity  Severity         `json:"s"`
	Log       SerializedString `json:"l"`
}

type StatEntry struct {
	Timestamp Timestamp `json:"ts"`
	CPU       float64   `json:"c"`
	Memory    uint64    `json:"m"`
}

type LogEntry struct {
	Type    LogEventType `json:"t"`
	Content interface{}  `json:"c"` // StructLogEntry or StatEntry
}

type ScrollIDs struct {
	ToOld int `json:"to_old,omitempty"`
	ToNew int `json:"to_new,omitempty"`
}

type FilterFunc func(entry interface{}) bool

type Option struct {
	Direction Direction
	Match     FilterFunc
}
