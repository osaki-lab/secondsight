package secondsight

import (
	"bufio"
	"context"
	"encoding/binary"
	"encoding/json"
	"errors"
	"fmt"
	"io"
	"log"
	"strings"
	"time"

	"github.com/docker/docker/api/types"
	"github.com/docker/docker/api/types/filters"
	"github.com/docker/docker/client"
)

type Event struct {
	EventType   DockerEventType
	ContainerID string
	Config      *ContainerConfig
	LogStream   <-chan LogEntry
}

type ContainerConfig struct {
	Name      string      `json:"name"`
	Active    bool        `json:"active"`
	ID        string      `json:"id"`
	Path      string      `json:"-"`
	CreatedAt Timestamp   `json:"createdAt"`
	UseTTY    bool        `json:"-"`
	Extra     [][2]string `json:"extra"`
}

type ContainerList struct {
	Containers []*ContainerConfig `json:"containers"`
}

type Docker interface {
	Events(ctx context.Context) (<-chan Event, error)
	Close() error
}

type docker struct {
	client      *client.Client
	cancelFuncs map[string]context.CancelFunc
}

func NewDocker() (Docker, error) {
	cli, err := client.NewClientWithOpts(client.FromEnv, client.WithAPIVersionNegotiation())
	if err != nil {
		return nil, err
	}
	return &docker{
		client:      cli,
		cancelFuncs: make(map[string]context.CancelFunc),
	}, nil
}

func (d docker) Close() error {
	return d.client.Close()
}

func (d *docker) createEvent(ctx context.Context, containerID string, eventType DockerEventType, since int64) (*Event, error) {
	config, err := d.container(ctx, containerID)
	if err != nil {
		return nil, fmt.Errorf("fail to get container: %w", err)
	}
	ctx, cancel := context.WithCancel(ctx)
	d.cancelFuncs[containerID] = cancel
	logEvents, err := d.listenContainerUpdate(ctx, containerID, config.UseTTY, since)
	if err != nil {
		return nil, fmt.Errorf("fail to get log: %w", err)
	}
	return &Event{
		EventType:   eventType,
		ContainerID: containerID,
		Config:      config,
		LogStream:   logEvents,
	}, nil
}

func (d *docker) Events(ctx context.Context) (<-chan Event, error) {
	now := time.Now()
	containers, err := d.client.ContainerList(ctx, types.ContainerListOptions{
		Before:  now.Format(time.RFC3339),
		Filters: filters.Args{},
	})
	if err != nil {
		return nil, err
	}
	events := make(chan Event)

	var runningEvents []Event

	for _, c := range containers {
		eventStream, err := d.createEvent(ctx, c.ID, ContainerRunningEvent, now.UnixNano())
		if err != nil {
			return nil, err
		}
		runningEvents = append(runningEvents, *eventStream)
	}
	go func() {
		for _, e := range runningEvents {
			events <- e
		}
		dockerEvents, _ := d.client.Events(
			ctx,
			types.EventsOptions{
				Since: now.Format(time.RFC3339),
				Filters: filters.NewArgs(
					filters.Arg("type", "container"),
				),
			},
		)
		for {
			select {
			case e := <-dockerEvents:
				switch e.Action {
				case "start":
					event, err := d.createEvent(ctx, e.ID, ContainerStartEvent, 0)
					if err != nil {
						log.Println(err)
						continue
					}
					events <- *event
				case "die":
					events <- Event{
						EventType:   ContainerDieEvent,
						ContainerID: e.ID,
					}
					cancel, ok := d.cancelFuncs[e.ID]
					if ok {
						cancel()
						delete(d.cancelFuncs, e.ID)
					} else {
						log.Println("💣 cancel func is not registered")
					}
				}
			case <-ctx.Done():
				close(events)
				return
			}
		}
	}()
	return events, nil
}

func (d docker) container(ctx context.Context, id string) (*ContainerConfig, error) {
	c, err := d.client.ContainerInspect(ctx, id)
	if err != nil {
		return nil, err
	}
	created, err := time.Parse(time.RFC3339, c.Created)
	if err != nil {
		return nil, err
	}
	extra := [][2]string{
		{
			"image", c.Image,
		},
		{
			"command", strings.Join(c.Config.Cmd, " "),
		},
		{
			"status", c.State.Status,
		},
	}

	return &ContainerConfig{
		Name:      c.Name,
		Active:    c.State.Running,
		ID:        c.ID,
		Path:      c.Path,
		CreatedAt: TimestampFromTime(created),
		UseTTY:    c.Config.Tty,
		Extra:     extra,
	}, nil
}

func (d docker) listenContainerUpdate(ctx context.Context, containerID string, useTTY bool, since int64) (<-chan LogEntry, error) {
	opt := types.ContainerLogsOptions{
		ShowStdout: true,
		ShowStderr: true,
		Timestamps: true,
		Follow:     true,
		Details:    false,
	}
	if since != 0 {
		u := time.Unix(0, since)
		opt.Since = u.Format(time.RFC3339)
	}
	rc, err := d.client.ContainerLogs(ctx, containerID, opt)
	if err != nil {
		return nil, err
	}
	logStream := make(chan LogEntry)
	go func() {
		<-ctx.Done()
		close(logStream)
	}()
	go func() {
		if useTTY {
			scanner := bufio.NewScanner(rc)
			for {
				if scanner.Scan() {
					if content, err := parseLog(scanner.Text()); err == nil {
						select {
						case <-ctx.Done():
							close(logStream)
							return
						case logStream <- LogEntry{
							Type:    StructLogEvent,
							Content: content,
						}:
							// do nothing
						}
					}
				} else {
					return
				}
			}
		} else {
			header := make([]byte, 8)
			for {
				_, err := io.ReadFull(rc, header)
				if err != nil {
					return
				}
				size := int(binary.BigEndian.Uint32(header[4:]))
				rawLog := make([]byte, size)
				_, err = io.ReadFull(rc, rawLog)
				if content, err := parseLog(strings.TrimRight(string(rawLog), "\n")); err == nil {
					select {
					case <-ctx.Done():
						close(logStream)
						return
					case logStream <- LogEntry{
						Type:    StructLogEvent,
						Content: content,
					}:
						// do nothing
					}
				}
			}
		}
	}()
	go func() {
		resp, err := d.client.ContainerStats(ctx, containerID, true)
		if err != nil {
			return
		}
		dec := json.NewDecoder(resp.Body)
		for {
			var v types.StatsJSON
			err := dec.Decode(&v)
			if err != nil {
				if errors.Is(err, context.DeadlineExceeded) || errors.Is(err, context.Canceled) {
					return
				}
				log.Println(err)
				return
			}
			var cpuPercent float64
			var mem uint64
			if resp.OSType != "windows" {
				cpuPercent = calculateCPUPercentUnix(v)
				mem = calculateMemUsageUnixNoCache(v)
			} else {
				cpuPercent = calculateCPUPercentWindows(v)
				mem = v.MemoryStats.PrivateWorkingSet
			}
			select {
			case <-ctx.Done():
				close(logStream)
				return
			case logStream <- LogEntry{
				Type: StatLogEvent,
				Content: &StatEntry{
					Timestamp: TimestampFromTime(v.Read),
					CPU:       cpuPercent,
					Memory:    mem,
				},
			}:
				// do nothing
			}
		}
	}()
	return logStream, nil
}

// based on https://github.com/docker/cli/command/container/stats_helpers.go
func calculateCPUPercentUnix(v types.StatsJSON) float64 {
	previousCPU := v.PreCPUStats.CPUUsage.TotalUsage
	previousSystem := v.PreCPUStats.SystemUsage
	var (
		cpuPercent = 0.0
		// calculate the change for the cpu usage of the container in between readings
		cpuDelta = float64(v.CPUStats.CPUUsage.TotalUsage) - float64(previousCPU)
		// calculate the change for the entire system between readings
		systemDelta = float64(v.CPUStats.SystemUsage) - float64(previousSystem)
		onlineCPUs  = float64(v.CPUStats.OnlineCPUs)
	)

	if onlineCPUs == 0.0 {
		onlineCPUs = float64(len(v.CPUStats.CPUUsage.PercpuUsage))
	}
	if systemDelta > 0.0 && cpuDelta > 0.0 {
		cpuPercent = (cpuDelta / systemDelta) * onlineCPUs * 100.0
	}
	return cpuPercent
}

// based on https://github.com/docker/cli/command/container/stats_helpers.go
func calculateCPUPercentWindows(v types.StatsJSON) float64 {
	// Max number of 100ns intervals between the previous time read and now
	possIntervals := uint64(v.Read.Sub(v.PreRead).Nanoseconds()) // Start with number of ns intervals
	possIntervals /= 100                                         // Convert to number of 100ns intervals
	possIntervals *= uint64(v.NumProcs)                          // Multiple by the number of processors

	// Intervals used
	intervalsUsed := v.CPUStats.CPUUsage.TotalUsage - v.PreCPUStats.CPUUsage.TotalUsage

	// Percentage avoiding divide-by-zero
	if possIntervals > 0 {
		return float64(intervalsUsed) / float64(possIntervals) * 100.0
	}
	return 0.00
}

// based on https://github.com/docker/cli/command/container/stats_helpers.go
func calculateMemUsageUnixNoCache(v types.StatsJSON) uint64 {
	mem := v.MemoryStats
	// cgroup v1
	if v, isCgroup1 := mem.Stats["total_inactive_file"]; isCgroup1 && v < mem.Usage {
		return mem.Usage - v
	}
	// cgroup v2
	if v := mem.Stats["inactive_file"]; v < mem.Usage {
		return mem.Usage - v
	}
	return mem.Usage
}

func getStrValue(m map[string]interface{}, key string) (string, bool) {
	if v, ok := m[key]; ok {
		if str, ok := v.(string); ok {
			return str, true
		}
	}
	return "", false
}

func getNumValue(m map[string]interface{}, key string) (int, bool) {
	if v, ok := m[key]; ok {
		if num, ok := v.(float64); ok {
			return int(num), true
		}
	}
	return 0, false
}

func parsePythonStyleLog(src string) (logger, message string, severity Severity, ok bool) {
	for k, v := range str2level {
		if strings.HasPrefix(src, k) {
			fragments := strings.SplitN(src[len(k):], ":", 3)
			if len(fragments) == 3 {
				return fragments[1], fragments[2], v, true
			}
		}
	}
	return "", "", InfoLevel, false
}

func parseLog(logString string) (entry StructLogEntry, err error) {
	severity := InfoLevel
	fragments := strings.SplitN(logString, " ", 2)
	if len(fragments) != 2 {
		err = fmt.Errorf("wrong format: %s", logString)
		return
	}
	var ts time.Time
	ts, err = time.Parse(time.RFC3339Nano, fragments[0])
	if err != nil {
		return
	}
	logSource := fragments[1]

	var jsonLog map[string]interface{}
	jsonErr := json.Unmarshal([]byte(logSource), &jsonLog)
	var spanID string
	var traceID string
	if jsonErr != nil {
		logger, msg, sev, ok := parsePythonStyleLog(logSource)
		if ok {
			jsonLog = map[string]interface{}{"message": msg, "logger": logger}
			severity = sev
		} else {
			jsonLog = map[string]interface{}{"message": logSource}
		}
	} else {
		// Cloud Logging Style severity field
		// https://cloud.google.com/logging/docs/reference/v2/rest/v2/LogEntry
		if v, ok := getStrValue(jsonLog, "severity"); ok {
			severity = ParseLevel(v, InfoLevel)
			delete(jsonLog, "severity")
		} else if v, ok := getStrValue(jsonLog, "level"); ok {
			// zerolog and zap style error level field
			// https://github.com/rs/zerolog
			// https://github.com/uber-go/zap
			severity = ParseLevel(v, InfoLevel)
			delete(jsonLog, "level")
		} else if v, ok := getNumValue(jsonLog, "level"); ok {
			// pino style error level field
			// https://getpino.io/
			severity, ok = num2level[v]
			if !ok {
				severity = InfoLevel
			}
			delete(jsonLog, "level")
		}
		// Cloud Logging Style trace/spanId field
		// https://cloud.google.com/logging/docs/reference/v2/rest/v2/LogEntry
		tv, ok1 := getStrValue(jsonLog, "trace")
		sv, ok2 := getStrValue(jsonLog, "spanId")
		if ok1 && ok2 {
			traceID = tv
			spanID = sv
			delete(jsonLog, "trace")
			delete(jsonLog, "spanId")
		}
	}
	jsonStr, err := json.Marshal(jsonLog)
	if err != nil {
		return
	}
	entry = StructLogEntry{
		TraceID:   traceID,
		SpanID:    spanID,
		Timestamp: TimestampFromTime(ts),
		Log:       SerializedString(jsonStr),
		Severity:  severity,
	}
	return
}
