# secondsight: structured docker log viewer


## Usage

It is a simple web server. You can modify port number by using PORT environment variable.

```shell
$ go get gitlab.com/osaki-lab/secondsight/cmd/secondsight
$ ./secondsight
```

This tool watches Docker's status via `/var/run/docker.sock` socket.

# License

Apache2

# Credits

* Icon:

  https://icomoon.io


