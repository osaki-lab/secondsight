export function formatDate(target: number, now: Date, short: boolean) {
    const t = new Date(target);
    if (now.getFullYear() !== t.getFullYear() || !short) {
        return `${t.getFullYear()}/${String(t.getMonth() + 1).padStart(
            2,
            '0'
        )}/${String(t.getDate()).padStart(2, '0')} ${String(
            t.getHours()
        ).padStart(2, '0')}:${String(t.getMinutes()).padStart(2, '0')}:${String(
            t.getSeconds()
        ).padStart(2, '0')}`;
    } else if (
        now.getMonth() !== t.getMonth() ||
        now.getDate() !== t.getDate()
    ) {
        return `${String(t.getMonth() + 1).padStart(2, '0')}/${String(
            t.getDate()
        ).padStart(2, '0')} ${String(t.getHours()).padStart(2, '0')}:${String(
            t.getMinutes()
        ).padStart(2, '0')}:${String(t.getSeconds()).padStart(2, '0')}`;
    } else {
        return `${String(t.getHours()).padStart(2, '0')}:${String(
            t.getMinutes()
        ).padStart(2, '0')}:${String(t.getSeconds()).padStart(2, '0')}`;
    }
}
