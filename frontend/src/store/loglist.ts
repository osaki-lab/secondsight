import { createSlice, PayloadAction } from '@reduxjs/toolkit';

export type Log = {
    traceID: { tid: string; sid: string };
    severity: number;
    time: number;
    log: any;
    logstr: string;
};

export type Stat = {
    time: number;
    cpu: number;
    memory: number;
};

type State = {
    containerID: string;
    logs: Log[];
    cpu: number[];
    memory: number[];
    statLabels: Date[];
    scrollIDs: { toNew: string; toOld: string };
    fetching: boolean;
    watching: boolean;
    closed: boolean;
    query: string;
};

const initialState: State = {
    containerID: '',
    logs: [],
    cpu: [],
    memory: [],
    statLabels: [],
    scrollIDs: { toNew: '', toOld: '' },
    fetching: false,
    watching: false,
    closed: false,
    query: '',
};

export const loglistSlice = createSlice({
    name: 'loglist',
    initialState,
    reducers: {
        setActiveLogList: (state, action: PayloadAction<string>) => {
            return {
                ...state,
                logs: [],
                containerID: action.payload,
            };
        },
        appendLog(
            state,
            action: PayloadAction<{ ch: string; logs: Log[]; stats: Stat[] }>
        ) {
            if (state.containerID !== action.payload.ch) {
                return state;
            }

            const cpu = action.payload.stats.map((s) => s.cpu * 100);
            const memory = action.payload.stats.map((s) => s.memory / 1000000);
            const statLabels = action.payload.stats.map(
                (s) => new Date(s.time)
            );

            return {
                ...state,
                logs: [...state.logs, ...action.payload.logs],
                cpu: [...state.cpu, ...cpu],
                memory: [...state.memory, ...memory],
                statLabels: [...state.statLabels, ...statLabels],
            };
        },
        prependLog(
            state,
            action: PayloadAction<{ ch: string; logs: Log[]; stats: Stat[] }>
        ) {
            if (state.containerID !== action.payload.ch) {
                return state;
            }

            const cpu = action.payload.stats.map((s) => s.cpu * 100);
            const memory = action.payload.stats.map((s) => s.memory / 1000000);
            const statLabels = action.payload.stats.map(
                (s) => new Date(s.time)
            );

            return {
                ...state,
                logs: [...action.payload.logs, ...state.logs],
                cpu: [...cpu, ...state.cpu],
                memory: [...memory, ...state.memory],
                statLabels: [...statLabels, ...state.statLabels],
            };
        },
    },
});

export const { setActiveLogList, appendLog, prependLog } = loglistSlice.actions;
