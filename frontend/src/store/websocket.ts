import { createAsyncThunk } from '@reduxjs/toolkit';
import type { RootState, Dispatch } from './index';
import { store } from './index';
import { updateContainerList } from './container';
import type { Container } from './container';
import {
    connectionStart,
    connectionError,
    connectionClose,
} from './connection';
import type { Log, Stat } from './loglist';
import { prependLog, appendLog } from './loglist';
import type { SemanticDiagnosticsBuilderProgram } from 'typescript';

type connectErrorType =
    | {
          error: false;
      }
    | {
          error: true;
      };

type StructLog = {
    l: any;
    tid: string;
    sid: string;
    s: number;
    ts: number;
};

type StatLog = {
    ts: number;
    c: number;
    m: number;
};

type ReceivedLog =
    | {
          t: 'l';
          c: StructLog;
      }
    | {
          t: 's';
          c: StatLog;
      };

type Channel = {
    name: string;
    type: string;
};

type ContainerResponse = {
    id: string;
    name: string;
    active: boolean;
    createdAt: number;
    extra: [string, string][];
};

function convertToContainer(input: ContainerResponse[]): Container[] {
    return input.map((c) => ({
        name: c.name,
        id: c.id,
        active: c.active,
        createdAt: c.createdAt,
        extra: c['extra'],
        unreadError: 0,
    }));
}

type WebSocketSendMessage =
    | {
          cmd: 'connect';
          chan: string;
          query?: { [key: string]: string };
      }
    | {
          cmd: 'disconnect' | 'old-log';
          chan: string;
      }
    | {
          cmd: 'channels';
      };

type WebSocketReceiveMessage = {
    cmd: 'new-log' | 'old-log' | 'container-list';
    chan?: string;
    query?: { [key: string]: string };
    logs: ReceivedLog[];
    containers?: ContainerResponse[];
    count?: number;
    scrollID: string;
};

function cmd(obj: WebSocketSendMessage): string {
    return JSON.stringify(obj);
}

function connectToChannel(
    socket: WebSocket,
    current: string | null,
    next: string | null
) {
    if (next === null && current !== null) {
        socket.send(
            cmd({
                cmd: 'disconnect',
                chan: current,
            })
        );
    } else if (next !== null && next !== current) {
        if (current !== null) {
            socket.send(
                cmd({
                    cmd: 'disconnect',
                    chan: current,
                })
            );
        }
        socket.send(
            cmd({
                cmd: 'connect',
                chan: next,
            })
        );
    }
}

function formatLog(
    receivedLogs: ReceivedLog[]
): { logs: Log[]; stats: Stat[] } {
    const now = new Date();
    const logs: Log[] = [];
    const stats: Stat[] = [];
    for (const log of receivedLogs) {
        if (log.t === 'l') {
            const { s, tid, sid, ts, l } = log.c;
            logs.push({
                severity: s,
                traceID: {
                    tid: tid,
                    sid: sid,
                },
                time: ts,
                log: l,
                logstr: JSON.stringify(l),
            });
        } else if (log.t === 's') {
            const { ts, c, m } = log.c;
            stats.push({
                time: ts,
                cpu: c,
                memory: m,
            });
        }
    }
    return { logs, stats };
}

function receiveMessage(event: MessageEvent, dispatch: Dispatch) {
    let msg: WebSocketReceiveMessage;
    try {
        msg = JSON.parse(event.data) as WebSocketReceiveMessage;
    } catch (e) {
        console.error(e);
        return;
    }
    switch (msg.cmd) {
        case 'new-log': {
            const { logs, stats } = formatLog(msg.logs);
            dispatch(appendLog({ ch: msg.chan as string, logs, stats }));
            break;
        }
        case 'old-log': {
            const { logs, stats } = formatLog(msg.logs);
            dispatch(prependLog({ ch: msg.chan as string, logs, stats }));
            break;
        }
        case 'container-list':
            const containers = msg.containers
                ? (msg.containers as ContainerResponse[])
                : ([] as ContainerResponse[]);
            dispatch(updateContainerList(convertToContainer(containers)));
            // do nothing;
            break;
    }
}

export const watchEventStream = createAsyncThunk<connectErrorType>(
    'container/watch',
    async (arg, thunk): Promise<connectErrorType> => {
        const { getState, dispatch } = thunk;
        if ((getState() as RootState).connection.connecting) {
            return { error: false };
        }
        dispatch(connectionStart);

        let socket: WebSocket;

        // detect active channel change
        let currentActiveChannel: string | null = null;
        store.subscribe(() => {
            const { active } = store.getState().connection;
            if (socket && currentActiveChannel !== active) {
                connectToChannel(socket, currentActiveChannel, active);
                currentActiveChannel = active;
            }
        });

        async function connection(): Promise<connectErrorType> {
            return new Promise<connectErrorType>((resolve) => {
                socket = new WebSocket(`ws://${location.host}/api/stream`);
                socket.onopen = function (e) {
                    console.log(e);
                };

                socket.onmessage = function (event) {
                    receiveMessage(event, dispatch);
                };

                socket.onclose = function (_event) {
                    dispatch(connectionClose());
                };

                socket.onerror = function (_error) {
                    dispatch(connectionError());
                };
            });
        }

        async function sleep(count: number) {
            return new Promise<void>((resolve) => {
                setTimeout(resolve, count * 1000);
            });
        }

        let retryCount = 0;
        for (;;) {
            const { error } = await connection();
            if (error) {
                retryCount++;
                if (retryCount > 5) {
                    return { error: true };
                }
            } else {
                retryCount = 0;
            }
            console.warn(`retry ${retryCount} seconds later`);
            await sleep(retryCount + 1);
        }
    }
);
