import { configureStore } from '@reduxjs/toolkit';
import { containerSlice } from './container';
import { loglistSlice } from './loglist';
import { connectionSlice } from './connection';
import {
    useSelector as useReduxSelector,
    TypedUseSelectorHook,
} from 'react-redux';

export const store = configureStore({
    reducer: {
        container: containerSlice.reducer,
        loglist: loglistSlice.reducer,
        connection: connectionSlice.reducer,
    },
});

export type RootState = ReturnType<typeof store.getState>;
export const useSelector: TypedUseSelectorHook<RootState> = useReduxSelector;
export type Dispatch = typeof store.dispatch;
