import { createSlice, PayloadAction } from '@reduxjs/toolkit';

export type State = {
    connecting: boolean;
    error: boolean;
    active: string | null;
};

const initialState: State = {
    connecting: false,
    error: false,
    active: null,
};

export const connectionSlice = createSlice({
    name: 'connection',
    initialState,
    reducers: {
        connectionStart(state) {
            return {
                ...state,
                connecting: true,
            };
        },
        connectionClose(state) {
            return {
                ...state,
                connecting: false,
            };
        },
        connectionError(state) {
            return {
                ...state,
                connecting: false,
                error: true,
            };
        },
        setActiveChannel(state, action: PayloadAction<string | null>) {
            return {
                ...state,
                active: action.payload,
            };
        },
    },
});

export const {
    setActiveChannel,
    connectionStart,
    connectionClose,
    connectionError,
} = connectionSlice.actions;
