import { createAction, createSlice, PayloadAction } from '@reduxjs/toolkit';

export type Container = {
    name: string;
    id: string;
    active: boolean;
    unreadError: number;
    createdAt: number;
    extra: [string, string][];
};

export type State = {
    containers: { [id: string]: Container };
    modelLabel: 'containers' | 'processes' | '';
};

const initialState: State = {
    containers: {},
    modelLabel: '',
};

// local action from WebSocket
export const updateContainerList = createAction<Container[]>(
    'container/update-container-list'
);

export const containerSlice = createSlice({
    name: 'container',
    initialState,
    reducers: {
        resetUnreadErrorCount(
            state: Readonly<State>,
            action: PayloadAction<string | null>
        ) {
            console.log('resetUnreadErrorCount', action.payload);
            const containers = { ...state.containers };
            if (action.payload !== null) {
                const container = containers[action.payload];
                containers[action.payload] = {
                    ...container,
                    unreadError: 0,
                };
            }
            return {
                ...state,
                containers,
                active: action.payload,
            };
        },
        addUnreadErrorCount(
            state: Readonly<State>,
            action: PayloadAction<{ id: string; num: number }>
        ) {
            const containers = { ...state.containers };
            const container = containers[action.payload.id];
            containers[action.payload.id] = {
                ...container,
                unreadError: container.unreadError + action.payload.num,
            };
            return {
                ...state,
                containers,
            };
        },
    },
    extraReducers(builder) {
        builder.addCase(updateContainerList, (state, action) => {
            const containers = {} as { [id: string]: Container };
            for (const c of action.payload) {
                containers[c.id] = {
                    name: c.name,
                    id: c.id,
                    active: c.active,
                    createdAt: c.createdAt,
                    extra: c['extra'],
                    unreadError: 0,
                };
            }
            return {
                ...state,
                containers,
                skipped: false,
            };
        });
    },
});

export const { addUnreadErrorCount } = containerSlice.actions;
