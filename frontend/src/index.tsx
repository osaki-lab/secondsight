import React, { StrictMode } from 'react';
import { render } from 'react-dom';
import { ThemeProvider, Arwes, createTheme } from '@arwes/arwes';
import { App } from './app';
import { store } from './store';
import { Provider } from 'react-redux';
import 'regenerator-runtime/runtime.js';

render(
    <StrictMode>
        <Provider store={store}>
            <ThemeProvider theme={createTheme()}>
                <Arwes>
                    <App />
                </Arwes>
            </ThemeProvider>
        </Provider>
    </StrictMode>,
    document.getElementById('root')
);
