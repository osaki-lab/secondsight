import React, { useEffect } from 'react';
import { useDispatch } from 'react-redux';
import { Route, Switch } from 'wouter';

import { ResizePanel } from './components/templates/resizepanel';
import { SideMenu } from './components/templates/sidemenu';

import type { Dispatch } from './store';
import { watchEventStream } from './store/websocket';

import { Description } from './pages/description';
import { LogViewer } from './pages/logviewer';

export function App() {
    const dispatch = useDispatch<Dispatch>();
    useEffect(() => {
        dispatch(watchEventStream());
    }, []);

    return (
        <ResizePanel>
            <SideMenu></SideMenu>
            <Switch>
                <Route path="/trace">
                    <h1>trace root</h1>
                </Route>
                {/*<Route path="/trace/:traceID">
                        <h1>trace page</h1>
                        </Route>*/}
                <Route path="/container/:id">
                    <LogViewer></LogViewer>
                </Route>
                <Route path="/">
                    <Description></Description>
                </Route>
            </Switch>
        </ResizePanel>
    );
}
