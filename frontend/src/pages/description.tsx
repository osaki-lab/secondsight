import React from 'react';
import { Content, Header } from '@arwes/arwes';

export function Description() {
    return (
        <Content style={{ margin: 20 }}>
            <Header animate show>
                <h1>Log Viewer for Local Development</h1>
            </Header>
        </Content>
    );
}
