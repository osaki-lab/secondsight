import React, { useState, useEffect, MouseEvent } from 'react';
import { useDispatch } from 'react-redux';
import { useRoute } from 'wouter';
import { style, classes } from 'typestyle';
import { useResizeDetector } from 'react-resize-detector';

import { useSelector, Dispatch } from '../store';
import { setActiveChannel } from '../store/connection';
import { setActiveLogList } from '../store/loglist';
import type { Log } from '../store/loglist';

import { ScrollView } from '../components/molecules/scrollview';
import { JSONFormatter } from '../components/atoms/jsonformatter';
import { SeverityIcon } from '../components/atoms/severityicon';
import { PopupDetailLogView } from '../components/molecules/popupdetaillogview';
import { formatDate } from '../utils/date';
import { StatsView } from '../components/molecules/statsview';

const verticalLayoutClass = style({
    display: 'flex',
    flexDirection: 'column',
    flexWrap: 'nowrap',
});

const logRowClass = style({
    display: 'flex',
    flexDirection: 'row',
    flexWrap: 'nowrap',
});

const standardRowClass = classes(
    logRowClass,
    style({
        $nest: {
            '&:hover': {
                backgroundColor: '#029dbb66',
                transition: '0.5s',
            },
        },
    })
);

const standardSelectedRowClass = classes(
    logRowClass,
    style({
        backgroundColor: '#5FD5EC33',
        $nest: {
            '&:hover': {
                backgroundColor: '#5FD5EC66',
                transition: '0.5s',
            },
        },
    })
);

const warningRowClass = classes(
    logRowClass,
    style({
        backgroundColor: '#FFCB1B33',
        $nest: {
            '&:hover': {
                backgroundColor: '#FFCB1B66',
                transition: '0.5s',
            },
        },
    })
);

const warningSelectedRowClass = classes(
    logRowClass,
    style({
        backgroundColor: '#FFE17C33',
        $nest: {
            '&:hover': {
                backgroundColor: '#FFE17C66',
                transition: '0.5s',
            },
        },
    })
);

const errorRowClass = classes(
    logRowClass,
    style({
        backgroundColor: '#FF7F7F33',
        $nest: {
            '&:hover': {
                backgroundColor: '#FF7F7F66',
                transition: '0.5s',
            },
        },
    })
);

const errorSelectedRowClass = classes(
    logRowClass,
    style({
        backgroundColor: '#FFA8A866',
        $nest: {
            '&:hover': {
                backgroundColor: '#FFA8A899',
                transition: '0.5s',
            },
        },
    })
);

function rowClass(severity: number, selected: boolean) {
    switch (severity) {
        case 4:
            if (selected) {
                return warningSelectedRowClass;
            } else {
                return warningRowClass;
            }
        case 5:
            if (selected) {
                return errorSelectedRowClass;
            } else {
                return errorRowClass;
            }
        default:
            if (selected) {
                return standardSelectedRowClass;
            } else {
                return standardRowClass;
            }
    }
}

const severitySpanClass = style({
    fontSize: '70%',
    flexGrow: 0,
    flexShrink: 0,
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
});

const dateSpanClass = style({
    fontSize: '70%',
    marginLeft: '0.5em',
    overflow: 'hidden',
    textOverflow: 'ellipsis',
    whiteSpace: 'nowrap',
    flexGrow: 0,
    flexShrink: 0,
});

const logSpanClass = style({
    fontSize: '70%',
    marginLeft: '1em',
    marginTop: '0em',
    marginBottom: '0em',
    overflow: 'hidden',
    textOverflow: 'ellipsis',
    whiteSpace: 'nowrap',
    flexGrow: 1,
    flexShrink: 1,
    $nest: {
        pre: {
            overflow: 'hidden',
            textOverflow: 'ellipsis',
            whiteSpace: 'nowrap',
        },
    },
});

export function LogViewer() {
    const dispatch = useDispatch<Dispatch>();

    const { id } = useRoute<{ id: string }>('/container/:id')[1] as {
        id: string;
    };
    const [windowHeight, setWindowHeight] = useState(window.innerHeight);

    const { width, ref } = useResizeDetector();

    const logs = useSelector((store) => store.loglist.logs);

    useEffect(() => {
        const handleResize = () => {
            setWindowHeight(window.innerHeight);
        };
        window.addEventListener('resize', handleResize);
        return () => {
            window.removeEventListener('resize', handleResize);
        };
    }, []);

    useEffect(() => {
        (async function () {
            dispatch(setActiveLogList(id));
            dispatch(setActiveChannel(id));
        })();
    }, [id]);

    const [selectedLog, setSelectedLog] = useState<Log | null>(null);

    const onClickLog = (e: MouseEvent) => {
        const target = e.currentTarget as HTMLDivElement;
        const index = target.getAttribute('data-index');
        setSelectedLog(logs[Number(index)]);
    };

    const now = new Date();

    return (
        <>
            <PopupDetailLogView
                log={selectedLog}
                onClose={() => {
                    setSelectedLog(null);
                }}
            ></PopupDetailLogView>
            <div className={verticalLayoutClass} ref={ref}>
                <StatsView width={width || 0}></StatsView>
                <ScrollView
                    logs={logs}
                    height={windowHeight - 38}
                    child={({ index, size, start }) => {
                        const log = logs[index];
                        return (
                            <div
                                key={index}
                                data-index={index}
                                className={rowClass(
                                    log.severity,
                                    log === selectedLog
                                )}
                                style={{
                                    position: 'absolute',
                                    top: 0,
                                    left: 0,
                                    width: '100%',
                                    height: `${size}px`,
                                    transform: `translateY(${start}px)`,
                                }}
                                onClick={onClickLog}
                            >
                                <span className={severitySpanClass}>
                                    <SeverityIcon
                                        severity={log.severity}
                                    ></SeverityIcon>
                                </span>
                                <span className={dateSpanClass}>
                                    {formatDate(log.time, now, true)}
                                </span>
                                <span className={logSpanClass}>
                                    <JSONFormatter
                                        space={1}
                                        inline={true}
                                        data={log.log}
                                    />
                                </span>
                            </div>
                        );
                    }}
                ></ScrollView>
            </div>
        </>
    );
}
