import React, { Fragment } from 'react';
import { style } from 'typestyle';
import { Heading, List } from '@arwes/arwes';
import { Link } from 'wouter';

import { useSelector } from '../../store';

const sidebarPanelClass = style({
    display: 'flex',
    height: '100%',
    flexDirection: 'column',
    overflowX: 'hidden',
    overflowY: 'scroll',
});

const sidebarMainMenuClass = style({
    flexGrow: 1,
});

const sidebarSubMenuClass = style({
    flexGrow: 0,
});

const tooltipBaseClass = style({
    position: 'relative',
    display: 'inline-block',
    $nest: {
        '& .tooltip': {
            minWidth: '400px',
            top: '40px',
            left: '50%',
            transform: 'translate(-10%, 0)',
            padding: '10px 20px',
            color: '#26dafd',
            backgroundColor: 'rgba(3,29,34,0.45)',
            position: 'absolute',
            zIndex: 1000,
            boxSizing: 'border-box',
            visibility: 'hidden',
            opacity: 0,
            transition: 'opacity 0.8s',
            backdropFilter: 'blur(4px)',
            border: '1px solid rgb(2, 157, 187)',
        },
        '&:hover .tooltip': {
            visibility: 'visible',
            opacity: 1,
        },
        '& .tooltip i': {
            position: 'absolute',
            bottom: '100%',
            left: '20%',
            marginLeft: '-12px',
            width: '24px',
            height: '12px',
            overflow: 'hidden',
        },
        '& .tooltip i:after': {
            content: '',
            position: 'absolute',
            width: '12px',
            height: '12px',
            left: '50%',
            transform: 'translate(-50%,50%) rotate(45deg)',
            backgroundColor: 'rgba(3,29,34,0.45)',
            backdropFilter: 'blur(4px)',
        },
    },
});

export function SideMenu() {
    const container = useSelector((state) => state.container);
    const sortedContainers = Object.values(container.containers);
    sortedContainers.sort((a, b) => a.createdAt - b.createdAt);

    return (
        <div className={sidebarPanelClass}>
            <div className={sidebarMainMenuClass}>
                <Heading node="h3">LOGS</Heading>
                {/*<Button layer="control">Travel to Space</Button>*/}
                <List node="ul">
                    {sortedContainers.map((container) => {
                        return (
                            <li key={container.id} className={tooltipBaseClass}>
                                <span
                                    style={{
                                        textDecoration: container.active
                                            ? ''
                                            : 'line-through',
                                        opacity: container.active ? 1 : 0.5,
                                    }}
                                >
                                    <Link
                                        to={`/container/${container.id}`}
                                        style={{
                                            color: 'inherit',
                                            textDecoration: 'inherit',
                                        }}
                                    >
                                        {container.name}
                                    </Link>
                                </span>
                                <div className="tooltip">
                                    <div>
                                        <List node="dl">
                                            <dt>Created At</dt>
                                            <dd>
                                                {new Date(
                                                    container.createdAt
                                                ).toLocaleDateString()}
                                            </dd>
                                            {container.extra.map(
                                                ([key, value]) => (
                                                    <Fragment key={key}>
                                                        <dt>{key}</dt>
                                                        <dd>{value}</dd>
                                                    </Fragment>
                                                )
                                            )}
                                        </List>
                                    </div>
                                    <i></i>
                                </div>
                            </li>
                        );
                    })}
                </List>
                <Heading node="h3">TRACE</Heading>
                <List node="ul">
                    <li>View Trace</li>
                </List>
            </div>
            <div className={sidebarSubMenuClass}>
                <Heading node="h3">MISC</Heading>
                <List node="ul">
                    <li>Config</li>
                    <li>About</li>
                </List>
            </div>
        </div>
    );
}
