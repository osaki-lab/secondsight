import React, { useState, ReactNode, MouseEvent } from 'react';
import { style } from 'typestyle';

const panelContainerClass = style({
    display: 'flex',
    height: '100%',
});

const panelClass = style({
    padding: '14px',
});

const resizerClass = style({
    width: '2px',
    position: 'relative',
    cursor: 'col-resize',
    flexShrink: 0,
    userSelect: 'none',
    $nest: {
        '&::before': {
            content: '',
            borderLeft: '1px solid',
            position: 'absolute',
            top: '50%',
            transform: 'translateY(-100%)',
            left: 0,
            right: 0,
            display: 'inline-block',
            height: '20px',
            margin: '0 2px',
        },
        '&::after': {
            content: '',
            borderLeft: '1px solid',
            position: 'absolute',
            top: '50%',
            transform: 'translateY(-100%)',
            right: 0,
            display: 'inline-block',
            height: '20px',
            margin: '0 2px',
        },
    },
});

export function ResizePanel(props: {
    children: ReactNode[];
    borderColor?: string;
}) {
    const [dragging, setDragging] = useState(false);
    const [width, setWidth] = useState(300);
    const [delta, setDelta] = useState(0);
    const [initialPos, setInitialPos] = useState(0);

    function startResize(event: MouseEvent) {
        setDragging(true);
        setInitialPos(event.clientX);
    }

    function stopResize() {
        if (dragging) {
            setDragging(false);
            setWidth(width + delta);
            setDelta(0);
        }
    }

    function resizePanel(event: MouseEvent) {
        if (dragging) {
            setDelta(event.clientX - initialPos);
        }
    }
    return (
        <div
            className={panelContainerClass}
            onMouseMove={resizePanel}
            onMouseUp={stopResize}
            onMouseLeave={stopResize}
        >
            <div className={panelClass} style={{ width: `${width}px` }}>
                {props.children[0]}
            </div>
            <div
                onMouseDown={startResize}
                key="resizer"
                style={{
                    left: dragging ? delta : undefined,
                    background: props.borderColor,
                }}
                className={resizerClass}
            ></div>
            <div
                className={panelClass}
                style={{ width: `calc(100% - ${width}px)` }}
            >
                {props.children[1]}
            </div>
        </div>
    );
}

ResizePanel.defaultProps = {
    borderColor: '#029dbb',
};
