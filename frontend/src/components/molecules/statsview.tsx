import React from 'react';
import { style } from 'typestyle';
import { Frame } from '@arwes/arwes';
import { Line } from 'react-chartjs-2';

import { useSelector } from '../../store';

type Props = {
    width: number;
};

const dialogBaseClass = style({
    position: 'absolute',
    top: 10,
    right: 10,
    height: 180,
});

const frameClass = style({
    height: '100%',
    display: 'flex',
    flexDirection: 'column',
});

export function StatsView({ width }: Props) {
    const { cpu, memory, statLabels } = useSelector((store) => store.loglist);

    const graphData = {
        labels: [...statLabels],
        datasets: [
            {
                type: 'line',
                yAxisID: 'y-axis-cpu',
                data: [...cpu],
                borderColor: 'rgba(255, 0, 0, 0.5)',
                label: 'CPU usage',
            },
            {
                type: 'line',
                yAxisID: 'y-axis-memory',
                data: [...memory],
                fill: true,
                backgroundColor: 'rgba(30, 144, 255, 0.5)',
                label: 'Memory usage',
            },
        ],
    };

    const graphOptions = {
        scales: {
            xAxes: [
                {
                    scaleLabel: {
                        display: true,
                        labelString: 'Time',
                    },
                    type: 'time',
                    time: {
                        displayFormats: {
                            quarter: 'h:mm:ss',
                        },
                    },
                },
            ],
            yAxes: [
                {
                    id: 'y-axis-cpu',
                    position: 'left',
                    scaleLabel: {
                        display: true,
                        labelString: 'CPU%',
                    },
                    ticks: {
                        beginAtZero: true,
                        callback: (value: number) => {
                            return `${value}%`;
                        },
                    },
                },
                {
                    id: 'y-axis-memory',
                    position: 'right',
                    scaleLabel: {
                        display: true,
                        labelString: 'Memory(MB)',
                    },
                    ticks: {
                        beginAtZero: true,
                        callback: (value: number) => {
                            return `${value}MB`;
                        },
                    },
                },
            ],
        },
        maintainAspectRatio: false,
    };

    return (
        <div className={dialogBaseClass} style={{ width: `${width}px` }}>
            <Frame
                show
                animate
                hover
                level={2}
                corners={4}
                layer="primary"
                className={frameClass}
            >
                <Line
                    height={180}
                    data={graphData}
                    options={graphOptions}
                ></Line>
            </Frame>
        </div>
    );
}
