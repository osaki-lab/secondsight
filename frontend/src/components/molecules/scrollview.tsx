import React, {
    useRef,
    useCallback,
    useState,
    useEffect,
    ReactElement,
} from 'react';
import { useVirtual } from 'react-virtual';

type Props<T> = {
    logs: T[];
    height: number;
    // eslint-disable-next-line no-unused-vars
    child: (opt: {
        index: number;
        size: number;
        start: number;
    }) => ReactElement;
};

export function ScrollView<T>(props: Props<T>) {
    const { logs, height, child } = props;
    const parentRef = useRef<HTMLDivElement>(null);

    const [lastItemCount, setLastItemCount] = useState(logs.length);

    const scrollToFn = useCallback((offset, defaultScrollTo) => {
        defaultScrollTo(offset);
    }, []);

    const {
        virtualItems,
        totalSize,
        scrollToIndex,
        // scrollToOffset,
    } = useVirtual({
        size: logs.length,
        parentRef,
        estimateSize: useCallback(() => 25, []),
        overscan: 5,
        scrollToFn,
    });

    useEffect(() => {
        if (parentRef.current === null) {
            return;
        }
        setLastItemCount(logs.length);
        const lastHeight = lastItemCount * 25;
        const offset =
            parentRef.current.scrollTop +
            parentRef.current.getBoundingClientRect().height;
        if (lastHeight - offset < 30) {
            scrollToIndex(logs.length - 1, { align: 'end' });
        }
    }, [logs.length]);

    return (
        <div
            ref={parentRef}
            className="List"
            style={{
                marginTop: '200px',
                height: `${height - 200}px`,
                width: `100%`,
                overflow: 'auto',
            }}
        >
            <div
                className="ListInner"
                style={{
                    height: `${totalSize}px`,
                    width: '100%',
                    position: 'relative',
                }}
            >
                {virtualItems.map(child)}
            </div>
        </div>
    );
}
