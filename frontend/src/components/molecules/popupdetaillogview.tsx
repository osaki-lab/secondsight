import React, { MouseEvent } from 'react';
import { Frame } from '@arwes/arwes';
import { style } from 'typestyle';

import type { Log } from '../../store/loglist';
import { JSONFormatter } from '../atoms/jsonformatter';
import { SeverityIcon } from '../atoms/severityicon';
import { formatDate } from '../../utils/date';

const dialogBaseClass = style({
    position: 'fixed',
    zIndex: 10,
    top: 210,
    right: 10,
    height: 'calc(100vh - 220px)',
});

const frameClass = style({
    height: '100%',
    backdropFilter: 'blur(6px)',
    display: 'flex',
    flexDirection: 'column',
});

const titleBaseClass = style({
    flexGrow: 1,
    display: 'flex',
    flexDirection: 'row',
    padding: '10px 20px',
    fontSize: '16px',
});

const titleSeverityClass = style({
    flexGrow: 0,
    flexShrink: 0,
    marginInlineEnd: '10px',
});

const titleClass = style({
    flexGrow: 1,
    flexShrink: 1,
});

const titleCloseButtonClass = style({
    flexGrow: 0,
    flexShrink: 0,
    width: '16px',
    height: '16px',
    $nest: {
        '&:hover': {
            backgroundColor: '#ffffff55',
            cursor: 'pointer',
        },
    },
});

const logSpanClass = style({
    fontSize: '70%',
    marginLeft: '1em',
    marginTop: '0em',
    marginBottom: '0em',
    overflow: 'hidden',
    textOverflow: 'ellipsis',
    $nest: {
        pre: {
            overflow: 'hidden',
            textOverflow: 'ellipsis',
        },
    },
});

type Props = {
    log: Log | null;
    onClose: () => void;
};

export function PopupDetailLogView(props: Props) {
    const { log, onClose } = props;
    if (!log) {
        return null;
    }
    const now = new Date();
    return (
        <div className={dialogBaseClass}>
            <Frame
                show
                animate
                hover
                level={2}
                corners={4}
                layer="primary"
                className={frameClass}
            >
                <div className={titleBaseClass}>
                    <span className={titleSeverityClass}>
                        <SeverityIcon severity={log.severity}></SeverityIcon>
                    </span>
                    <span className={titleClass}>
                        {formatDate(log.time, now, false)}
                    </span>
                    <span
                        className={titleCloseButtonClass}
                        onClick={(e: MouseEvent) => {
                            e.preventDefault();
                            onClose();
                        }}
                    >
                        ✖️
                    </span>
                </div>
                <div className={logSpanClass}>
                    <JSONFormatter inline={false} space={2} data={log.log} />
                </div>
            </Frame>
        </div>
    );
}
