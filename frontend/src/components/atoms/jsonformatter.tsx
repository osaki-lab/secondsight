/* eslint-disable @typescript-eslint/no-explicit-any */
import React from 'react';

// based on https://github.com/chenckang/react-json-pretty/blob/master/src/JSONFormatter.tsx

type Theme = {
    [key: string]: string;
};

export const themes: { [key: string]: Theme } = {
    acai: {
        main: 'color:#748096;overflow:auto;',
        error: 'color:#748096;overflow:auto;',
        key: 'color:#b553bf;',
        string: 'color:#fba856;',
        value: 'color:#93a3bf;',
        boolean: 'color:#448aa9;',
    },
};

type Props = React.HTMLAttributes<HTMLElement> & {
    data?: any;
    replacer?: (_key: string, _value: any) => any | null;
    space?: number | string;
    themeClassName?: string;
    theme?: Theme;
    silent?: boolean;
    onJSONFormatterError?: (_e: Error) => void;
    mainStyle?: string;
    keyStyle?: string;
    stringStyle?: string;
    valueStyle?: string;
    booleanStyle?: string;
    errorStyle?: string;
    inline?: boolean;
};

function getStyleValue(name: string, theme: Theme, styles: any): string {
    const extra = styles[name + 'Style'] || '';
    const style = theme ? theme[name] || '' : '';
    return extra ? `${extra};${style}` : style;
}

function getStyle(name: string, theme: Theme, styles: any): string {
    const value = getStyleValue(name, theme, styles);
    return value ? ` style="${value}"` : '';
}

/*
const xssmap: { [key: string]: string } = {
    '"': '&quot;',
    "'": '&apos;',
    '&': '&amp;',
    '>': '&gt;',
    '<': '&lt',
};

function xss(s: string): string {
    if (!s) {
        return s;
    }

    return s.replace(/<|>|&|"|'/g, (m) => {
        return xssmap[m];
    });
}
*/

// JSON =》 HTML转换器
function pretty(
    theme: Theme,
    obj: any,
    space: number,
    styles: any,
    replacer?: (_k: string, _v: any) => any
) {
    const regLine = /^( *)("[^"]+": )?("[^"]*"|[\w.+-]*)?([,[{]|\[\s*\],?|\{\s*\},?)?$/gm;
    const text = JSON.stringify(obj, replacer, isNaN(space) ? 2 : space);

    /* istanbul ignore next */
    if (!text) {
        return text;
    }

    return text
        .replace(/&/g, '&amp;')
        .replace(/\\"([^,])/g, '\\&quot;$1')
        .replace(/</g, '&lt;')
        .replace(/>/g, '&gt;')
        .replace(regLine, replace.bind(null, theme, styles));
}

// 格式化函数
function replace(
    theme: Theme,
    styles: any,
    match: any,
    ind: string,
    key: string,
    val: string,
    tra: string
) {
    const spanEnd = '</span>';
    const keySpan = `<span class="__json-key__"${getStyle(
        'key',
        theme,
        styles
    )}>`;
    const valSpan = `<span class="__json-value__"${getStyle(
        'value',
        theme,
        styles
    )}>`;
    const strSpan = `<span class="__json-string__"${getStyle(
        'string',
        theme,
        styles
    )}>`;
    const booSpan = `<span class="__json-boolean__"${getStyle(
        'boolean',
        theme,
        styles
    )}>`;

    let sps = ind || '';
    if (key) {
        sps =
            sps +
            '"' +
            keySpan +
            key.replace(/^"|":\s$/g, '') +
            spanEnd +
            '": ';
    }

    if (val) {
        if (val === 'true' || val === 'false') {
            sps = sps + booSpan + val + spanEnd;
        } else {
            sps = sps + (val[0] === '"' ? strSpan : valSpan) + val + spanEnd;
        }
    }

    return sps + (tra || '');
}

export function JSONFormatter(props: Props) {
    const {
        data,
        replacer,
        space,
        themeClassName,
        theme = themes.acai,
        // onJSONFormatterError,
        // onError,
        // silent,
        mainStyle,
        keyStyle,
        valueStyle,
        stringStyle,
        booleanStyle,
        errorStyle,
        inline,
        ...rest
    } = props;

    let normalizedSpace: number;
    if (space === undefined) {
        normalizedSpace = 0;
    } else if (typeof space === 'number') {
        normalizedSpace = space;
    } else {
        normalizedSpace = space.length;
    }

    const styles = {
        mainStyle,
        keyStyle,
        valueStyle,
        stringStyle,
        booleanStyle,
        errorStyle,
    };

    return inline ? (
        <div
            style={{ display: 'inline-block' }}
            {...rest}
            dangerouslySetInnerHTML={{
                __html: `<span style="margin: 0em;display: inline; white-space: nowrap;" class="${themeClassName}"${getStyle(
                    'main',
                    theme,
                    styles
                )}>${pretty(
                    theme,
                    data,
                    normalizedSpace,
                    styles,
                    replacer
                )}</span>`,
            }}
        ></div>
    ) : (
        <div
            {...rest}
            dangerouslySetInnerHTML={{
                __html: `<pre class="${themeClassName}"${getStyle(
                    'main',
                    theme,
                    styles
                )}>${pretty(
                    theme,
                    data,
                    normalizedSpace,
                    styles,
                    replacer
                )}</pre>`,
            }}
        ></div>
    );
}

JSONFormatter.defaultProps = {
    data: '',
    json: '',
    silent: true,
    space: 2,
    themeClassName: '__json-pretty__',
};
