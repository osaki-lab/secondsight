const ParcelProxyServer = require("parcel-proxy-server");
const { dirname, join } = require("path");

// configure the proxy server
const server = new ParcelProxyServer({
    entryPoint: join(dirname(__dirname), "src", "index.html"),
    parcelOptions: {
        // provide parcel options here
        // these are directly passed into the
        // parcel bundler
        //
        // More info on supported options are documented at
        // https://parceljs.org/api
        port: 1234,
        open: true,
    },
    proxies: {
        // add proxies here
        "/api": {
            target: "http://localhost:55555/",
        },
    },
});

// the underlying parcel bundler is exposed on the server
// and can be used if needed
server.bundler.on("buildEnd", () => {
    console.log("Build completed!");
});

// start up the server
server.listen(8080, () => {
    console.log("Parcel proxy server has started");
});
