declare module "@arwes/arwes" {
    import React, { Element, HTMLAttributes, SFC } from "react";
    import { ThemeProvider, withTheme } from "theming";

    export const ThemeProvider;
    export const withTheme;
    type color = {
        base: string;
        light: string;
        dark: string;
    };
    type backgroundColor = {
        level0: string;
        level1: string;
        level2: string;
        level3: string;
    };

    type theme = Partial<{
        margin: number;
        padding: number;
        // Base box or text shadow length.
        shadowLength: number;
        // Base animation duration in ms.
        animTime: number;
        // The opacity to apply to elements when needed.
        alpha: number;
        // The color variation.
        accent: number;
        // Every color has a `base`, `light` and `dark` variation.
        color: {
            primary: color;
            secondary: color;
            header: color;
            control: color;
            success: color;
            alert: color;
            disabled: color;
        };
        // Every background color has level colors from 0 until 3
        // as `level0`, `level1`...
        background: {
            primary: backgroundColor;
            secondary: backgroundColor;
            header: backgroundColor;
            control: backgroundColor;
            success: backgroundColor;
            alert: backgroundColor;
            disabled: backgroundColor;
        };
        typography: Partial<{
            lineHeight: number;
            headerSizes: Partial<{
                h1: number;
                h2: number;
                h3: number;
                h4: number;
                h5: number;
                h6: number;
            }>;
            fontSize: number;
            headerFontFamily: string;
            fontFamily: string;
        }>;
        code: Partial<{
            fontSize: number;
            fontFamily: string;
            background: string;
            color: string;
            comment: string;
            keyword: string;
            operator: string;
            function: string;
            variable: string;
            selector: string;
            value: string;
        }>;
        // Number of columns inside a row
        columns: number;
        // Until the number in device screen width the breakpoint is taken.
        // E.g. Until the `small` number is viewport small, from `small + 1` is medium.
        // After `large` one it is `xlarge`.
        responsive: Partial<{
            small: number;
            medium: number;
            large: number;
        }>;
    }>;

    export function createTheme(theme?: theme): theme;

    type ArwesProps = Partial<
        React.HTMLAttributes<Element> & {
            Animation: any;
            Puffs: any;
            createResponsive: any;
            createLoader: any;
            animate?: boolean;
            show?: boolean;
            animation?: any;
            background?:
                | string
                | {
                      small: string;
                      medium: string;
                      large: string;
                      xlarge: string;
                  };
            pattern?:
                | string
                | {
                      small: string;
                      medium: string;
                      large: string;
                      xlarge: string;
                  };
            loadResources?: boolean;
            showResources?: boolean;
            puffsProps?: any;
            className?: string;
            rest?: any;
            children?: Element | Element[];
        }
    >;
    export const Arwes: SFC<ArwesProps>;

    export const Content: SFC<HTMLAttributes<Element>>;

    type HeaderProps = Partial<
        HTMLAttributes<Element> & {
            Animation: any;
            animate?: boolean;
            show?: boolean;
            animation?: any;
            sounds?: any;
            className?: string;
            etc?: any;
        }
    >;
    export const Header: SFC<HeaderProps>;

    type HeadingProps = {
        node: "h1" | "h2" | "h3" | "h4" | "h5" | "h6";
        children: Element | Element[];
        className?: string;
    };
    export const Heading: SFC<HeadingProps>;

    type ListProps = {
        node: "ul" | "ol" | "dl";
        children: Element | Element[];
        className?: string;
    };
    export const List: SFC<ListProps>;

    type ButtonProps = Partial<
        HTMLAttributes<Element> & {
            Animation: any;
            Highlight: any;
            Frame: any;
            animate?: boolean;
            show?: boolean;
            animation?: any;
            sounds?: any;
            layer?:
                | "primary"
                | "secondary"
                | "header"
                | "control"
                | "success"
                | "alert"
                | "disabled";
            disabled?: boolean;
            active?: boolean;
            level?: number;
            buttonProps?: any;
            className?: any;
            onClick?: (...args: any[]) => any;
            etc?: any;
        }
    >;
    export const Button: SFC<ButtonProps>;

    type FrameProps = Partial<
        HTMLAttributes<Element> & {
            Animation: any;
            animate?: boolean;
            show?: boolean;
            animation?: any;
            sounds?: any;
            layer?:
                | "primary"
                | "secondary"
                | "header"
                | "control"
                | "success"
                | "alert"
                | "disabled";
            level?: 0 | 1 | 2 | 3;
            corners?: 0 | 1 | 2 | 3 | 4 | 5 | 6;
            border?: boolean;
            disabled?: boolean;
            active?: boolean;
            hover?: boolean;
            noBackground?: boolean;
            className?: any;
            etc?: any;
        }
    >;
    export const Frame: SFC<FrameProps>;

    type ProjectProps = Partial<{
        theme;
        sounds?: any;
        Animation: any;
        Frame?: Frame;
        Words?: any;
        Heading?: Heading;
        animation?: any;
        animate?: boolean;
        show?: boolean;
        node?: string;
        header: string;
        headerSize?: "h1" | "h2" | "h3" | "h4" | "h5" | "h6";
        icon?: any;
        children?: Element | Element[];
        etc?: any;
    }>;
    export const Project: SFC<ProjectProps>;
}
