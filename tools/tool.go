package main

//go:generate gocredits .. -f ../CREDITS

import "fmt"

func main() {
	fmt.Println(`Usage:
    $ go generate
`)
}
