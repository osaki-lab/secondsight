package secondsight

import (
	"context"
	"log"
	"sync"
	"time"
)

type ClientConnections struct {
	conns []*ClientConnection
	lock  *sync.RWMutex
}

func (c *ClientConnections) Register(ctx context.Context, conn *ClientConnection) {
	c.lock.Lock()
	defer c.lock.Unlock()
	c.conns = append(c.conns, conn)
	go func() {
		<-ctx.Done()
		c.lock.Lock()
		defer c.lock.Unlock()
		conns := make([]*ClientConnection, 0, len(c.conns)-1)
		for _, conn := range c.conns {
			if conn != conn {
				conns = append(conns, conn)
			}
		}
		c.conns = conns
	}()
}

func (c *ClientConnections) Loop(callback func(*ClientConnection)) {
	c.lock.RLock()
	defer c.lock.RUnlock()
	for _, conn := range c.conns {
		callback(conn)
	}
}

func (c *ClientConnections) ReceiveLogUpdate(channelName string) {
	now := time.Now().UnixNano()
	c.Loop(func(c *ClientConnection) {
		c.receiveLogUpdate(now, channelName)
	})
}

func NewClientConnections() *ClientConnections {
	return &ClientConnections{
		lock: &sync.RWMutex{},
	}
}

// ClientConnection is a backbone of client side API
//
// WebSocketConnection is built on ClientConnection
type ClientConnection struct {
	receiver     Receiver
	logStore     *LogStore
	scrollIDs    map[string]ScrollIDs
	timeFilter   int64
}

func NewClientConnection(receiver Receiver, logStore *LogStore) *ClientConnection {
	result := &ClientConnection{
		scrollIDs:    make(map[string]ScrollIDs),
		logStore:     logStore,
		receiver:     receiver,
		timeFilter:   time.Now().UnixNano(),
	}

	// client automatically receive the recent channel list
	result.updateChannels(result.timeFilter, nil)
	return result
}

func (c *ClientConnection) Listen(channelName string, filterSrc map[string]string) error {
	c.scrollIDs[channelName] = ScrollIDs{
		ToOld: -1,
		ToNew: -1,
	}
	c.RetrieveLogs(channelName)
	return nil
}

func (c *ClientConnection) Disconnect(containerID string) {
	delete(c.scrollIDs, containerID)
}

func (c *ClientConnection) RetrieveLogs(containerID string) error {
	result, scrollIDs, err := c.logStore.Search(containerID, c.scrollIDs[containerID].ToOld, 50, Option{
		Direction: NewToOld,
	})
	if err != nil {
		return err
	}
	scrollIDs.ToNew = c.scrollIDs[containerID].ToNew
	c.scrollIDs[containerID] = scrollIDs
	c.receiver.ReceiveOldLogs(containerID, result, scrollIDs.ToOld)
	return nil
}

func (c *ClientConnection) RetrieveChannels() {
	var containerIDs []string
	var containers []*ContainerConfig
	c.logStore.Iterate(func(containerID string, container *Container) {
		containerIDs = append(containerIDs, containerID)
		containers = append(containers, container.config)
	})
	c.receiver.ReceiveChannelUpdate(containerIDs, containers)
}

func (c *ClientConnection) updateChannels(timestamp int64, updated []string) {
	if timestamp < c.timeFilter {
		return
	}
	scrollIDs := make(map[string]ScrollIDs)
	var containers []*ContainerConfig
	c.logStore.Iterate(func(containerID string, container *Container) {
		if scrollID, exists := c.scrollIDs[containerID]; exists {
			scrollIDs[containerID] = scrollID
		}
		containers = append(containers, container.config)
	})
	c.scrollIDs = scrollIDs
	c.receiver.ReceiveChannelUpdate(updated, containers)
}

func (c *ClientConnection) receiveLogUpdate(timestamp int64, containerID string) {
	if timestamp < c.timeFilter {
		return
	}
	if scrollIDs, ok := c.scrollIDs[containerID]; ok {
		result, scrollIDs, err := c.logStore.Search(containerID, scrollIDs.ToNew, 100, Option{
			Direction: OldTONew,
		})
		if err != nil {
			log.Println(err)
		}
		c.scrollIDs[containerID] = ScrollIDs{
			ToNew: scrollIDs.ToNew,
			ToOld: c.scrollIDs[containerID].ToOld,
		}
		c.receiver.ReceiveLogUpdate(containerID, result, scrollIDs.ToNew)
	}
}
