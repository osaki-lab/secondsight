package secondsight

import (
	"bytes"
	"embed"
	"errors"
	"io"
	"log"
	"net/http"
	"path"

	"github.com/gabriel-vasile/mimetype"
)

//go:embed frontend/dist/*
var assets embed.FS

var ErrDir = errors.New("path is dir")

func tryRead(fs embed.FS, prefix, requestedPath string, w http.ResponseWriter, setCache bool) error {
	f, err := fs.Open(path.Join(prefix, requestedPath))
	if err != nil {
		log.Println(err)
		return err
	}
	defer f.Close()
	stat, _ := f.Stat()
	if stat.IsDir() {
		return ErrDir
	}
	var contentType string
	var buffer bytes.Buffer
	reader := io.TeeReader(f, &buffer)
	if m, err := mimetype.DetectReader(reader); err == nil {
		contentType = m.String()
	} else {
		contentType = "application/octet-stream"
	}
	w.Header().Set("Content-Type", contentType)
	if setCache {
		w.Header().Set("Cache-Control", "public, max-age=604800, immutable")
	}
	io.Copy(w, io.MultiReader(&buffer, f))
	return nil
}

func notFoundHandler(fs embed.FS, prefix, fallback string) func(w http.ResponseWriter, r *http.Request) {
	return func(w http.ResponseWriter, r *http.Request) {
		err := tryRead(fs, prefix, r.URL.Path, w, true)
		if err == nil {
			return
		}
		err = tryRead(fs, prefix, fallback, w, true)
		if err != nil {
			panic(err)
		}
	}
}

func NotFoundHandler(fallback string) func(w http.ResponseWriter, r *http.Request)  {
	return notFoundHandler(assets, "frontend/dist", fallback)
}