// +build pprof

package main

import (
	"fmt"
	"net/http"
	_ "net/http/pprof"
	"os"
)

func init() {
	go func() {
		fmt.Fprintln(os.Stderr, "launch: go tool pprof -http=\":8081\" http://localhost:6060/debug/pprof/profile for profiling")
		fmt.Fprintln(os.Stderr, http.ListenAndServe("localhost:6060", nil))
	}()
}