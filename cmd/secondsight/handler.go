package main

import (
	"encoding/json"
	"gitlab.com/osaki-lab/secondsight"
	"net/http"

	"github.com/go-chi/chi/v5"
)

type errorWriter struct {
	Message string `json:"message"`
	Error   string `json:"error"`
}

type Handler struct {
	connections *secondsight.ClientConnections
	logStore    *secondsight.LogStore
	docker      secondsight.Docker
}

func NewHandler(connections *secondsight.ClientConnections, logStore *secondsight.LogStore, docker secondsight.Docker) http.Handler {
	h := &Handler{
		connections: connections,
		logStore:    logStore,
		docker:      docker,
	}
	router := chi.NewRouter()

	router.Route("/api", func(r chi.Router) {
		r.Get("/stream", h.eventListener)
	})

	router.NotFound(secondsight.NotFoundHandler("index.html"))

	return router
}

func (h *Handler) eventListener(w http.ResponseWriter, r *http.Request) {
	wsConn, clientConn, err := secondsight.NewWebSocketConnection(w, r, h.logStore)
	if err == nil {
		h.connections.Register(r.Context(), clientConn)
		wsConn.Wait()
	} else {
		w.WriteHeader(http.StatusBadRequest)
		e := errorWriter{
			Message: "connection error",
			Error:   err.Error(),
		}
		enc := json.NewEncoder(w)
		enc.Encode(&e)
	}
}
