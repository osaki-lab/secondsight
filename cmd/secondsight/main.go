package main

import (
	"context"
	"fmt"
	"net/http"
	"os"
	"os/signal"
	"strconv"
	"time"

	"github.com/kelseyhightower/envconfig"
	"gitlab.com/osaki-lab/secondsight"
)

type Env struct {
	Port uint16 `envconfig:"PORT" default:"55555"`
}

func main() {
	var env Env
	err := envconfig.Process("", &env)
	if err != nil {
		fmt.Fprintf(os.Stderr, "Can't parse environment variables: %s\n", err.Error())
		os.Exit(1)
	}

	ctx, cancel := signal.NotifyContext(context.Background(), os.Interrupt, os.Kill)
	defer cancel()

	conns := secondsight.NewClientConnections()

	docker, err := secondsight.NewDocker()
	if err != nil {
		fmt.Fprintf(os.Stderr, "Can't initialize secondsight: %s\n", err.Error())
		os.Exit(1)
	}

	ls := secondsight.NewLogStore(conns)
	go func() {
		err = ls.ConnectToDocker(ctx, docker)
		if err != nil {
			fmt.Fprintln(os.Stderr, err.Error())
			os.Exit(1)
		}
		if ctx.Err() != nil {
			fmt.Fprintln(os.Stderr, "🚦 receive signal")
			os.Exit(0)
		}
	}()

	server := &http.Server{
		Addr:    ":" + strconv.FormatUint(uint64(env.Port), 10),
		Handler: NewHandler(conns, ls, docker),
	}

	go func() {
		<-ctx.Done()
		ctx, cancel := context.WithTimeout(context.Background(), 5*time.Second)
		defer cancel()
		server.Shutdown(ctx)
	}()
	fmt.Printf("start receiving at :%d", env.Port)
	fmt.Fprintln(os.Stderr, server.ListenAndServe())
}
