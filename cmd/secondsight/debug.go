// +build debug

package main

import "log"

func init() {
	log.SetPrefix("🐙 ")
	log.SetFlags(log.Lshortfile | log.LstdFlags)
}

